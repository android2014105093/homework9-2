package cwy.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

    int mCount = 0;

    public MyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.

        String act = intent.getAction();
        mCount++;

        if (act.equals(Intent.ACTION_BATTERY_CHANGED)) {
            Log.i("WatchSystem",
                    "BR" + mCount + ": ACTION_BATTERY_CHANGED "
                            + intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
                            + "%");
            Toast.makeText(context, "Battery Charged", Toast.LENGTH_SHORT).show();
        }
        else if (act.equals(Intent.ACTION_BATTERY_LOW)) {
            Log.i("WatchSystem", "BR" + mCount + ": ACTION_BATTERY_LOW");
            Toast.makeText(context, "Low Battery", Toast.LENGTH_SHORT).show();
        }
        else if (act.equals(Intent.ACTION_BATTERY_OKAY)) {
            Log.i("WatchSystem", "BR" + mCount + ": ACTION_BATTERY_OKAY");
            Toast.makeText(context, "Battery Okay", Toast.LENGTH_SHORT).show();
        }
        else if (act.equals(Intent.ACTION_POWER_CONNECTED)) {
            Log.i("WatchSystem", "BR" + mCount + ": ACTION_POWER_CONNECTED");
            Toast.makeText(context, "Battery Power Connected", Toast.LENGTH_SHORT).show();
        }
        else if (act.equals(Intent.ACTION_POWER_DISCONNECTED)) {
            Log.i("WatchSystem", "BR" + mCount + ": ACTION_POWER_DISCONNECTED");
            Toast.makeText(context, "Battery Power Disconnected", Toast.LENGTH_SHORT).show();
        }
        else if (act.equals(Intent.ACTION_MEDIA_MOUNTED)) {
            Log.i("WatchSystem", "BR" + mCount + ": ACTION_MEDIA_MOUNTED");
            Toast.makeText(context, "Media Mounted", Toast.LENGTH_SHORT).show();
        }
        else if (act.equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
            Log.i("WatchSystem", "BR" + mCount + ": ACTION_MEDIA_UNMOUNTED");
            Toast.makeText(context, "Media Unmounted", Toast.LENGTH_SHORT).show();
        }
    }
}
